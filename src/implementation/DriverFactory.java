package implementation;

import framework.User;
import framework.UserFactory;

public class DriverFactory implements UserFactory {
    @Override
    public User createUser(String name, String phoneNumber, String cid, String password) {
        return new Driver(name, phoneNumber, cid, password);
    }
}
