package implementation;

import framework.UserValidator;

public class ConcreteUserValidator extends UserValidator {
    @Override
    protected boolean validateName(String name) {
        return name.matches("[a-zA-Z]+");
    }

    @Override
    protected boolean validatePhoneNumber(String phoneNumber) {
        return phoneNumber.matches("^(17|77)\\d{6}$");
    }

    @Override
    protected boolean validateCid(String cid) {
        return cid.matches("^1\\d{10}$");
    }

    @Override
    protected boolean validatePassword(String password, String confirmPassword) {
        return password.length() == 8 && password.equals(confirmPassword);
    }
}
