package implementation;

import framework.User;

public class Passenger extends User {
    public Passenger(String name, String phoneNumber, String cid, String password) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.cid = cid;
        this.password = password;
        this.userRole = "passenger";
    }
}
