package implementation;

import framework.Tour;
import framework.TourBuilder;
import framework.UserSession;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TourBuilderImpl implements TourBuilder {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/taxi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    @Override
    public void buildItinerary(String tourName, String pickupPoint, String destination, String date, String time,
            String capacity, String amount) {
        // Convert date to the correct format (assuming date format is MM/DD/YYYY)
        String[] parts = date.split("/");
        String formattedDate = parts[2] + "-" + parts[0] + "-" + parts[1];

        // Retrieve phone number from UserSession
        UserSession userSession = UserSession.getInstance();
        String phoneNumber = userSession.getPhoneNumber();
        System.out.println(phoneNumber);
        // Fetch user name from the database based on phone number
        String userName = null;
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "SELECT name FROM users WHERE phone = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, phoneNumber);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                userName = resultSet.getString("name");
            } else {
                // Handle the scenario when the user is not found
                System.err.println("Error: User not found in the database.");
                return; // Exit method
            }
        } catch (SQLException e) {
            System.err.println("Error fetching user details from the database: " + e.getMessage());
            return; // Exit method
        }

        // Create and save the tour with all details
        Tour tour = new Tour(tourName, pickupPoint, destination, formattedDate, time, capacity, amount, userName,
                phoneNumber);
        saveTourToDatabase(tour);
    }

    @Override
    public void updateItinerary(Tour tour) {
        // Implement tour itinerary update logic
    }

    @Override
    public void deleteItinerary(Tour tour) {
        // Implement tour itinerary deletion logic
    }

    private void saveTourToDatabase(Tour tour) {
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "INSERT INTO tour (tour_name, pickup_point, destination, date, time, capacity, amount, driver_name, driver_phone_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, tour.getTourName());
            statement.setString(2, tour.getPickupPoint());
            statement.setString(3, tour.getDestination());
            statement.setString(4, tour.getDate());
            statement.setString(5, tour.getTime());
            statement.setString(6, tour.getCapacity());
            statement.setDouble(7, Double.parseDouble(tour.getAmount())); // Assuming amount is a String
            statement.setString(8, tour.getDriverName());
            statement.setString(9, tour.getDriverPhoneNumber());

            int rowsInserted = statement.executeUpdate();
            if (rowsInserted > 0) {
                System.out.println("Tour saved to the database.");
            }
        } catch (SQLException e) {
            System.err.println("Error saving tour to the database: " + e.getMessage());
        }
    }
}
