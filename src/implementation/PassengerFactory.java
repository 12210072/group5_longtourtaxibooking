package implementation;

import framework.User;
import framework.UserFactory;

public class PassengerFactory implements UserFactory {
    @Override
    public User createUser(String name, String phoneNumber, String cid, String password) {
        return new Passenger(name, phoneNumber, cid, password);
    }
}
