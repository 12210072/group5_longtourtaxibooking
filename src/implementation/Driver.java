package implementation;

import framework.User;

public class Driver extends User {
    public Driver(String name, String phoneNumber, String cid, String password) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.cid = cid;
        this.password = password;
        this.userRole = "driver";
    }
}
