package implementation;

import java.sql.*;

public class Booking {
    private String passengerPhoneNumber;
    private String driverPhoneNumber;
    private String tourName;
    private String date;
    private String time;
    private String status;

    public Booking(String passengerPhoneNumber, String driverPhoneNumber, String tourName,
            String date, String time, String status) {
        this.passengerPhoneNumber = passengerPhoneNumber;
        this.driverPhoneNumber = driverPhoneNumber;
        this.tourName = tourName;
        this.date = date;
        this.time = time;
        this.status = status;
    }

    public String getTourName() {
        return tourName;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getPassengerPhoneNumber() {
        return passengerPhoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public String confirm() {
        this.status = "confirmed";
        return this.status;
    }

    public String reject() {
        this.status = "rejected";
        return this.status;
    }

    public void updateStatusInDatabase() {
        String dbUrl = "jdbc:mysql://localhost:3306/taxi";
        String dbUser = "root";
        String dbPassword = "";

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword)) {
            String sql = "UPDATE booking SET status = ? WHERE passenger_phone_number = ? " +
                    "AND driver_phone_number = ? AND tour_name = ? AND date = ? AND time = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, status);
                statement.setString(2, passengerPhoneNumber);
                statement.setString(3, driverPhoneNumber);
                statement.setString(4, tourName);
                statement.setString(5, date);
                statement.setString(6, time);
                int rowsUpdated = statement.executeUpdate();
                if (rowsUpdated > 0) {
                    System.out.println("Booking status updated successfully.");
                } else {
                    System.out.println("No booking found for the given criteria.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getDriverPhoneNumber() {
        return driverPhoneNumber;
    }
}
