package main;

import javax.swing.*;
import java.awt.*;

public class LandingPage extends JFrame {
    public LandingPage() {
        setTitle("Taxi App");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JLabel welcomeLabel = new JLabel("Welcome to Taxi App", JLabel.CENTER);
        add(welcomeLabel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        JButton registerButton = new JButton("Register");
        JButton loginButton = new JButton("Login");

        registerButton.addActionListener(e -> {
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.setVisible(true);
            this.dispose();
        });

        loginButton.addActionListener(e -> {
            LoginForm loginForm = new LoginForm();
            loginForm.setVisible(true);
            this.dispose();
        });

        buttonPanel.add(registerButton);
        buttonPanel.add(loginButton);
        add(buttonPanel, BorderLayout.SOUTH);
    }
}
