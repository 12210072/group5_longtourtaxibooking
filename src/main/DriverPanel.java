package main;

import framework.Tour;
import framework.User;
import framework.UserSession;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DriverPanel extends JFrame {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/taxi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    public DriverPanel(User user) {
        setTitle("Driver Panel");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JLabel welcomeLabel = new JLabel("Hi, " + user.getName(), JLabel.CENTER);
        add(welcomeLabel, BorderLayout.NORTH);

        JPanel buttonPanel = new JPanel(new GridLayout(1, 3)); // Changed to accommodate new button

        JButton createTourButton = new JButton("Create Tour");
        createTourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TourForm().setVisible(true);
            }
        });
        buttonPanel.add(createTourButton);

        JButton myTourButton = new JButton("View Tours");
        myTourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Tour> tours = getDriverTours(user.getPhoneNumber());
                if (tours != null && !tours.isEmpty()) {
                    new BookedToursDialog(DriverPanel.this, tours).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(DriverPanel.this, "No tours found.", "My Tours",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        buttonPanel.add(myTourButton);

        // Add new button to view booking table
        JButton viewBookingButton = new JButton("View Bookings");
        viewBookingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new BookingDetailsFrame(user.getPhoneNumber()).setVisible(true);
            }
        });
        buttonPanel.add(viewBookingButton);

        add(buttonPanel, BorderLayout.CENTER);

        JButton logoutButton = new JButton("Logout");
        logoutButton.addActionListener(e -> {
            UserSession.getInstance().logout();
            new LandingPage().setVisible(true);
            dispose();
        });
        add(logoutButton, BorderLayout.SOUTH);
    }

    private List<Tour> getDriverTours(String phoneNumber) {
        List<Tour> tours = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "SELECT * FROM tour WHERE driver_phone_number = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, phoneNumber);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String tourName = resultSet.getString("tour_name");
                String pickupPoint = resultSet.getString("pickup_point");
                String destination = resultSet.getString("destination");
                String date = resultSet.getString("date");
                String time = resultSet.getString("time");
                String capacity = resultSet.getString("capacity");
                String amount = resultSet.getString("amount");
                String driverName = resultSet.getString("driver_name");
                String driverPhoneNumber = resultSet.getString("driver_phone_number");
                Tour tour = new Tour(tourName, pickupPoint, destination, date, time, capacity, amount, driverName,
                        driverPhoneNumber);
                tours.add(tour);
            }
        } catch (SQLException ex) {
            System.err.println("Error fetching tours from the database: " + ex.getMessage());
        }
        return tours;
    }
}
