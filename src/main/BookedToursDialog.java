package main;

import framework.Tour;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class BookedToursDialog extends JDialog {
    private List<Tour> bookedTours;

    public BookedToursDialog(Frame owner, List<Tour> bookedTours) {
        super(owner, "Booked Tours", true);
        this.bookedTours = bookedTours;

        setSize(600, 400);
        setLocationRelativeTo(owner);

        initComponents();
    }

    private void initComponents() {
        JPanel panel = new JPanel(new BorderLayout());
        String[] columnNames = { "Tour Name", "Pickup Point", "Destination", "Date", "Time", "Capacity", "Amount" };
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (Tour tour : bookedTours) {
            Object[] rowData = { tour.getTourName(), tour.getPickupPoint(), tour.getDestination(), tour.getDate(),
                    tour.getTime(), tour.getCapacity(), tour.getAmount() };
            model.addRow(rowData);
        }
        JTable table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, BorderLayout.CENTER);

        // Remove the button panel and its components
        // JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        // JButton confirmButton = new JButton("Confirm");
        // confirmButton.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // // Implement confirmation logic
        // JOptionPane.showMessageDialog(BookedToursDialog.this, "Bookings confirmed.");
        // dispose();
        // }
        // });
        // buttonPanel.add(confirmButton);

        // JButton rejectButton = new JButton("Reject");
        // rejectButton.addActionListener(new ActionListener() {
        // @Override
        // public void actionPerformed(ActionEvent e) {
        // // Implement rejection logic
        // JOptionPane.showMessageDialog(BookedToursDialog.this, "Bookings rejected.");
        // dispose();
        // }
        // });
        // buttonPanel.add(rejectButton);

        // panel.add(buttonPanel, BorderLayout.SOUTH);

        getContentPane().add(panel);
    }
}
