package main;

import framework.Tour;
import framework.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ToursFrame extends JFrame {
    private List<Tour> tours;
    private int currentIndex;

    private JPanel tourPanel;
    private JButton previousButton;
    private JButton nextButton;
    private JButton bookButton;

    private static final String DB_URL = "jdbc:mysql://localhost:3306/taxi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    public ToursFrame(List<Tour> tours) {
        this.tours = tours;
        this.currentIndex = 0;

        setTitle("Tours");
        setSize(600, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout(10, 10));

        JPanel navigationPanel = new JPanel(new FlowLayout());
        navigationPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        previousButton = new JButton("Previous");
        previousButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentIndex > 0) {
                    currentIndex--;
                    displayTour();
                }
            }
        });
        navigationPanel.add(previousButton);

        nextButton = new JButton("Next");
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentIndex < tours.size() - 1) {
                    currentIndex++;
                    displayTour();
                }
            }
        });
        navigationPanel.add(nextButton);

        add(navigationPanel, BorderLayout.NORTH);

        tourPanel = new JPanel(new GridLayout(9, 2, 10, 10));
        tourPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        add(tourPanel, BorderLayout.CENTER);

        displayTour();

        bookButton = new JButton("Book");
        bookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tour selectedTour = tours.get(currentIndex);
                String passengerPhoneNumber = UserSession.getInstance().getPhoneNumber();
                String driverPhoneNumber = selectedTour.getDriverPhoneNumber();
                insertBookingData(passengerPhoneNumber, driverPhoneNumber);
            }
        });
        JPanel bookPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        bookPanel.add(bookButton);
        add(bookPanel, BorderLayout.SOUTH);

        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void displayTour() {
        tourPanel.removeAll();
        Tour tour = tours.get(currentIndex);

        tourPanel.add(new JLabel("Tour Name:"));
        tourPanel.add(new JLabel(tour.getTourName()));

        tourPanel.add(new JLabel("Pickup Point:"));
        tourPanel.add(new JLabel(tour.getPickupPoint()));

        tourPanel.add(new JLabel("Destination:"));
        tourPanel.add(new JLabel(tour.getDestination()));

        tourPanel.add(new JLabel("Date:"));
        tourPanel.add(new JLabel(tour.getDate()));

        tourPanel.add(new JLabel("Time:"));
        tourPanel.add(new JLabel(tour.getTime()));

        tourPanel.add(new JLabel("Capacity:"));
        tourPanel.add(new JLabel(tour.getCapacity()));

        tourPanel.add(new JLabel("Amount:"));
        tourPanel.add(new JLabel(tour.getAmount()));

        tourPanel.add(new JLabel("Driver Name:"));
        tourPanel.add(new JLabel(tour.getDriverName()));

        tourPanel.add(new JLabel("Driver Phone Number:"));
        tourPanel.add(new JLabel(tour.getDriverPhoneNumber()));

        tourPanel.revalidate();
        tourPanel.repaint();
    }

    private void insertBookingData(String passengerPhoneNumber, String driverPhoneNumber) {
        String selectTourSql = "SELECT tour_name, date, time FROM tour WHERE id = ?";
        String insertBookingSql = "INSERT INTO booking (tour_name, date, time, passenger_phone_number, driver_phone_number, status) VALUES (?, ?, ?, ?, ?, 'pending')";

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
                PreparedStatement selectStatement = connection.prepareStatement(selectTourSql);
                PreparedStatement insertStatement = connection.prepareStatement(insertBookingSql)) {

            int tourId = currentIndex + 1;

            selectStatement.setInt(1, tourId);
            ResultSet resultSet = selectStatement.executeQuery();
            if (resultSet.next()) {
                String tourName = resultSet.getString("tour_name");
                String date = resultSet.getString("date");
                String time = resultSet.getString("time");

                insertStatement.setString(1, tourName);
                insertStatement.setString(2, date);
                insertStatement.setString(3, time);
                insertStatement.setString(4, passengerPhoneNumber);
                insertStatement.setString(5, driverPhoneNumber);

                insertStatement.executeUpdate();

                JOptionPane.showMessageDialog(ToursFrame.this, "Tour booked successfully!", "Booking",
                        JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(ToursFrame.this, "Failed to retrieve tour details.", "Booking",
                        JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(ToursFrame.this, "Failed to book tour.", "Booking",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
