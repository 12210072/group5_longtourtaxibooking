package main;

import database.UserDAO;
import framework.User;
import implementation.*;

import javax.swing.*;
import java.awt.*;

public class RegistrationForm extends JFrame {
    private JTextField nameField;
    private JTextField phoneField;
    private JTextField cidField;
    private JPasswordField passwordField;
    private JPasswordField confirmPasswordField;
    private JComboBox<String> userRoleBox;

    public RegistrationForm() {
        setTitle("Registration Form");
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(7, 2));

        nameField = new JTextField();
        phoneField = new JTextField();
        cidField = new JTextField();
        passwordField = new JPasswordField();
        confirmPasswordField = new JPasswordField();
        userRoleBox = new JComboBox<>(new String[] { "driver", "passenger" });

        add(new JLabel("Name:"));
        add(nameField);
        add(new JLabel("Phone:"));
        add(phoneField);
        add(new JLabel("CID:"));
        add(cidField);
        add(new JLabel("Password:"));
        add(passwordField);
        add(new JLabel("Confirm Password:"));
        add(confirmPasswordField);
        add(new JLabel("Role:"));
        add(userRoleBox);

        JButton registerButton = new JButton("Register");
        registerButton.addActionListener(e -> registerUser());

        add(registerButton);
    }

    private void registerUser() {
        String name = nameField.getText();
        String phone = phoneField.getText();
        String cid = cidField.getText();
        String password = new String(passwordField.getPassword());
        String confirmPassword = new String(confirmPasswordField.getPassword());
        String userRole = (String) userRoleBox.getSelectedItem();

        ConcreteUserValidator validator = new ConcreteUserValidator();

        if (!validator.validateUser(name, phone, cid, password, confirmPassword)) {
            JOptionPane.showMessageDialog(this, "Invalid data. Please check your inputs.");
            return;
        }

        User user;
        if ("driver".equals(userRole)) {
            user = new DriverFactory().createUser(name, phone, cid, password);
        } else {
            user = new PassengerFactory().createUser(name, phone, cid, password);
        }

        UserDAO userDAO = new UserDAO();
        if (userDAO.registerUser(user)) {
            JOptionPane.showMessageDialog(this, "Registration successful. Please login.");
            new LoginForm().setVisible(true);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Registration failed. Try again.");
        }
    }
}
