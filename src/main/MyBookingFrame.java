package main;

import javax.swing.*;

import implementation.Booking;

import java.awt.*;
import java.util.List;

public class MyBookingFrame extends JFrame {
    public MyBookingFrame(List<Booking> bookings) {
        setTitle("My Bookings");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel bookingPanel = new JPanel(new GridLayout(bookings.size(), 1));

        for (Booking booking : bookings) {
            JPanel bookingDetailsPanel = new JPanel(new GridLayout(6, 2));
            bookingDetailsPanel.setBorder(BorderFactory.createTitledBorder("Booking Details"));

            bookingDetailsPanel.add(new JLabel("Tour Name:"));
            bookingDetailsPanel.add(new JLabel(booking.getTourName()));

            bookingDetailsPanel.add(new JLabel("Date:"));
            bookingDetailsPanel.add(new JLabel(booking.getDate()));

            bookingDetailsPanel.add(new JLabel("Time:"));
            bookingDetailsPanel.add(new JLabel(booking.getTime()));

            bookingDetailsPanel.add(new JLabel("Passenger Phone Number:"));
            bookingDetailsPanel.add(new JLabel(booking.getPassengerPhoneNumber()));

            bookingDetailsPanel.add(new JLabel("Driver Phone Number:"));
            bookingDetailsPanel.add(new JLabel(booking.getDriverPhoneNumber()));

            bookingDetailsPanel.add(new JLabel("Status:"));
            bookingDetailsPanel.add(new JLabel(booking.getStatus()));

            bookingPanel.add(bookingDetailsPanel);
        }

        JScrollPane scrollPane = new JScrollPane(bookingPanel);
        add(scrollPane);

        setLocationRelativeTo(null);
        setVisible(true);
    }
}
