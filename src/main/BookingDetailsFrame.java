package main;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import implementation.Booking;

public class BookingDetailsFrame extends JFrame {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/taxi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    private int currentPosition = 0;
    private int totalRecords;
    private Booking currentBooking;

    private JTable bookingTable;
    private DefaultTableModel tableModel;

    public BookingDetailsFrame(String driverPhoneNumber) {
        setTitle("Booking Details");
        setSize(600, 400);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());

        // Top panel for buttons
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton prevButton = new JButton("Previous");
        JButton nextButton = new JButton("Next");
        topPanel.add(prevButton);
        topPanel.add(nextButton);
        add(topPanel, BorderLayout.NORTH);

        // Center panel for booking details using JTable
        JPanel centerPanel = new JPanel(new GridLayout(5, 2));
        tableModel = new DefaultTableModel();
        tableModel.addColumn("Tour Name");
        tableModel.addColumn("Date");
        tableModel.addColumn("Time");
        tableModel.addColumn("Passenger Phone");
        tableModel.addColumn("Status");
        bookingTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(bookingTable);
        centerPanel.add(scrollPane);
        add(centerPanel, BorderLayout.CENTER);

        // Bottom panel for dropdown and action button
        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        String[] actions = { "Confirm", "Reject" };
        JComboBox<String> actionDropdown = new JComboBox<>(actions);
        JButton actionButton = new JButton("Submit");
        bottomPanel.add(actionDropdown);
        bottomPanel.add(actionButton);
        add(bottomPanel, BorderLayout.SOUTH);

        // Action listeners
        prevButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPosition > 0) {
                    currentPosition--;
                    loadBookingDetails(driverPhoneNumber);
                }
            }
        });

        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentPosition < totalRecords - 1) {
                    currentPosition++;
                    loadBookingDetails(driverPhoneNumber);
                }
            }
        });

        actionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedAction = (String) actionDropdown.getSelectedItem();
                if (selectedAction != null && currentBooking != null) {
                    handleAction(selectedAction);
                }
            }
        });

        loadBookingDetails(driverPhoneNumber);
    }

    private void loadBookingDetails(String driverPhoneNumber) {
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            // Get total record count
            String countSql = "SELECT COUNT(*) AS total FROM booking WHERE driver_phone_number = ?";
            PreparedStatement countStatement = connection.prepareStatement(countSql);
            countStatement.setString(1, driverPhoneNumber);
            ResultSet countResultSet = countStatement.executeQuery();
            if (countResultSet.next()) {
                totalRecords = countResultSet.getInt("total");
            }

            // Get the record for the current position
            String sql = "SELECT * FROM booking WHERE driver_phone_number = ? LIMIT 1 OFFSET ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, driverPhoneNumber);
            statement.setInt(2, currentPosition);
            ResultSet resultSet = statement.executeQuery();

            // Clear existing data in the table
            tableModel.setRowCount(0);

            // Populate the table with booking details
            while (resultSet.next()) {
                String tourName = resultSet.getString("tour_name");
                String date = resultSet.getString("date");
                String time = resultSet.getString("time");
                String passengerPhoneNumber = resultSet.getString("passenger_phone_number");
                String status = resultSet.getString("status");

                tableModel.addRow(new Object[] { tourName, date, time, passengerPhoneNumber, status });

                // Create the current booking object
                currentBooking = new Booking(passengerPhoneNumber, driverPhoneNumber, tourName, date, time, status);
            }
        } catch (SQLException ex) {
            System.err.println("Error fetching booking data: " + ex.getMessage());
        }
    }

    private void handleAction(String action) {
        if (action.equals("Confirm")) {
            currentBooking.confirm();
            currentBooking.updateStatusInDatabase(); // Update status to confirmed in the database
        } else if (action.equals("Reject")) {
            currentBooking.reject();
            currentBooking.updateStatusInDatabase(); // Update status to rejected in the database
        }
        JOptionPane.showMessageDialog(this, "Booking status updated to " + action.toLowerCase() + ".");
        loadBookingDetails(currentBooking.getDriverPhoneNumber()); // Refresh booking details
    }
}
