package main;

import javax.swing.*;

import framework.Tour;
import framework.User;
import framework.UserSession;
import implementation.Booking;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PassengerPanel extends JFrame {
    private List<Booking> bookings;

    private static final String DB_URL = "jdbc:mysql://localhost:3306/taxi";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "";

    public PassengerPanel(User user) {
        setTitle("Passenger Panel");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JLabel welcomeLabel = new JLabel("Hi, " + user.getName(), JLabel.CENTER);
        add(welcomeLabel, BorderLayout.NORTH);

        JButton viewToursButton = new JButton("View Tours");
        viewToursButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Tour> tours = getToursFromDatabase();
                if (tours != null && !tours.isEmpty()) {
                    new ToursFrame(tours).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(PassengerPanel.this, "No tours found.", "Tours",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        JButton viewMyBookingButton = new JButton("View My Bookings");
        viewMyBookingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bookings = getBookingsFromDatabase(user.getPhoneNumber());
                if (bookings != null && !bookings.isEmpty()) {
                    new MyBookingFrame(bookings).setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(PassengerPanel.this, "No bookings found for this user.",
                            "Booking Details", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

        JButton logoutButton = new JButton("Logout");
        logoutButton.addActionListener(e -> {
            UserSession.getInstance().logout();
            new LandingPage().setVisible(true);
            dispose();
        });

        JPanel buttonPanel = new JPanel(new GridLayout(1, 3));
        buttonPanel.add(viewToursButton);
        buttonPanel.add(viewMyBookingButton);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.add(logoutButton);

        add(buttonPanel, BorderLayout.CENTER);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    private List<Tour> getToursFromDatabase() {
        List<Tour> tours = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "SELECT * FROM tour";
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String tourName = resultSet.getString("tour_name");
                String pickupPoint = resultSet.getString("pickup_point");
                String destination = resultSet.getString("destination");
                String date = resultSet.getString("date");
                String time = resultSet.getString("time");
                String capacity = resultSet.getString("capacity");
                String amount = resultSet.getString("amount");
                String driverName = resultSet.getString("driver_name");
                String driverPhoneNumber = resultSet.getString("driver_phone_number");
                Tour tour = new Tour(tourName, pickupPoint, destination, date, time, capacity, amount,
                        driverName, driverPhoneNumber);
                tours.add(tour);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return tours;
    }

    private List<Booking> getBookingsFromDatabase(String userPhoneNumber) {
        List<Booking> bookings = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {
            String sql = "SELECT * FROM booking WHERE passenger_phone_number = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, userPhoneNumber);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String passengerPhoneNumber = resultSet.getString("passenger_phone_number");
                String driverPhoneNumber = resultSet.getString("driver_phone_number");
                String tourName = resultSet.getString("tour_name");
                String date = resultSet.getString("date");
                String time = resultSet.getString("time");
                String status = resultSet.getString("status");
                Booking booking = new Booking(passengerPhoneNumber, driverPhoneNumber, tourName, date, time, status);
                bookings.add(booking);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return bookings;
    }
}
