package main;

import framework.TourBuilder;
import implementation.TourBuilderImpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TourForm extends JFrame {
    private JTextField tourNameField;
    private JTextField pickupPointField;
    private JTextField destinationField;
    private JTextField dateField;
    private JTextField timeField;
    private JTextField capacityField;
    private JTextField amountField;

    public TourForm() {
        setTitle("Create Tour");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayout(new GridLayout(8, 2));

        JLabel tourNameLabel = new JLabel("Tour Name:");
        tourNameField = new JTextField();
        add(tourNameLabel);
        add(tourNameField);

        JLabel pickupPointLabel = new JLabel("Pickup Point:");
        pickupPointField = new JTextField();
        add(pickupPointLabel);
        add(pickupPointField);

        JLabel destinationLabel = new JLabel("Destination:");
        destinationField = new JTextField();
        add(destinationLabel);
        add(destinationField);

        JLabel dateLabel = new JLabel("Date:");
        dateField = new JTextField();
        add(dateLabel);
        add(dateField);

        JLabel timeLabel = new JLabel("Time:");
        timeField = new JTextField();
        add(timeLabel);
        add(timeField);

        JLabel capacityLabel = new JLabel("Capacity:");
        capacityField = new JTextField();
        add(capacityLabel);
        add(capacityField);

        JLabel amountLabel = new JLabel("Amount:");
        amountField = new JTextField();
        add(amountLabel);
        add(amountField);

        JButton createTourButton = new JButton("Create Tour");
        createTourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String tourName = tourNameField.getText();
                String pickupPoint = pickupPointField.getText();
                String destination = destinationField.getText();
                String date = dateField.getText();
                String time = timeField.getText();
                String capacity = capacityField.getText();
                double amount = Double.parseDouble(amountField.getText());

                // Convert the amount to a String
                String amountString = String.valueOf(amount);

                // Instantiate TourBuilderImpl
                TourBuilder tourBuilder = new TourBuilderImpl();

                // Create the tour and save it to the database
                // Modify the buildItinerary method to accept the amount as a double
                tourBuilder.buildItinerary(tourName, pickupPoint, destination, date, time, capacity, amountString);

                // Close the tour form after creating the tour
                dispose();
            }
        });
        add(createTourButton);
    }
}
