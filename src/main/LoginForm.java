package main;

import database.UserDAO;
import framework.User;
import framework.UserSession;

import javax.swing.*;
import java.awt.*;

public class LoginForm extends JFrame {
    private JTextField phoneField;
    private JPasswordField passwordField;

    public LoginForm() {
        setTitle("Login Form");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(3, 2));

        phoneField = new JTextField();
        passwordField = new JPasswordField();

        add(new JLabel("Phone:"));
        add(phoneField);
        add(new JLabel("Password:"));
        add(passwordField);

        JButton loginButton = new JButton("Login");
        loginButton.addActionListener(e -> loginUser());

        add(loginButton);
    }

    private void loginUser() {
        String phone = phoneField.getText();
        String password = new String(passwordField.getPassword());

        UserDAO userDAO = new UserDAO();
        User user = userDAO.loginUser(phone, password);

        if (user != null) {
            UserSession.getInstance().login(phone);
            JOptionPane.showMessageDialog(this, "Login successful.");

            if ("driver".equals(user.getUserRole())) {
                new DriverPanel(user).setVisible(true);
            } else {
                new PassengerPanel(user).setVisible(true);
            }
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(this, "Invalid phone or password.");
        }
    }
}
