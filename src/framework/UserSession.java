package framework;

public class UserSession {
    private static UserSession instance;
    private String phoneNumber;

    private UserSession() {
    }

    public static UserSession getInstance() {
        if (instance == null) {
            instance = new UserSession();
        }
        return instance;
    }

    public void login(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void logout() {
        this.phoneNumber = null;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
