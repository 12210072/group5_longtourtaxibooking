package framework;

public class Tour {
    private String tourName;
    private String pickupPoint;
    private String destination;
    private String date;
    private String time;
    private String capacity;
    private String amount;
    private String driverName;
    private String driverPhoneNumber;

    public Tour(String tourName, String pickupPoint, String destination, String date, String time, String capacity,
            String amount, String driverName, String driverPhoneNumber) {
        this.tourName = tourName;
        this.pickupPoint = pickupPoint;
        this.destination = destination;
        this.date = date;
        this.time = time;
        this.capacity = capacity;
        this.amount = amount;
        this.driverName = driverName;
        this.driverPhoneNumber = driverPhoneNumber;
    }

    // Getter and setter methods for 'tourName'
    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    // Getter and setter methods for 'pickupPoint'
    public String getPickupPoint() {
        return pickupPoint;
    }

    public void setPickupPoint(String pickupPoint) {
        this.pickupPoint = pickupPoint;
    }

    // Getter and setter methods for 'destination'
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    // Getter and setter methods for 'date'
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    // Getter and setter methods for 'time'
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    // Getter and setter methods for 'capacity'
    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    // Getter and setter methods for 'amount'
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    // Getter and setter methods for 'driverName'
    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    // Getter and setter methods for 'driverPhoneNumber'
    public String getDriverPhoneNumber() {
        return driverPhoneNumber;
    }

    public void setDriverPhoneNumber(String driverPhoneNumber) {
        this.driverPhoneNumber = driverPhoneNumber;
    }

}
