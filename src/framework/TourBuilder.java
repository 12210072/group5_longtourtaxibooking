package framework;

public interface TourBuilder {
    void buildItinerary(String tourName, String pickupPoint, String destination, String date, String time,
            String capacity, String amount);

    void updateItinerary(Tour tour);

    void deleteItinerary(Tour tour);
}
