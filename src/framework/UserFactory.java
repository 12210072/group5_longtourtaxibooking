package framework;

// import User;

public interface UserFactory {
    User createUser(String name, String phoneNumber, String cid, String password);
}
