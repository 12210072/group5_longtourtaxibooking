// framework/BookingState.java
package framework;

import implementation.Booking;

public interface BookingState {
    void confirm(Booking booking);

    void reject(Booking booking);

    void cancel();
}
