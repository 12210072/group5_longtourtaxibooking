package framework;

public abstract class UserValidator {
    public final boolean validateUser(String name, String phoneNumber, String cid, String password,
            String confirmPassword) {
        return validateName(name) && validatePhoneNumber(phoneNumber) && validateCid(cid)
                && validatePassword(password, confirmPassword);
    }

    protected abstract boolean validateName(String name);

    protected abstract boolean validatePhoneNumber(String phoneNumber);

    protected abstract boolean validateCid(String cid);

    protected abstract boolean validatePassword(String password, String confirmPassword);
}
